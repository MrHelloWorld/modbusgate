/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ModbusTCPListener.cpp
 * Author: m
 * 
 * Created on 15 июня 2020 г., 17:20
 */

#include "ModbusTCPListener.h"
#include <ostream>
#include <iostream>
#include "Helper.h"
#include <ctime>

ModbusTCPListener::ModbusTCPListener(int port): port(port)
{
    addrLenght = sizeof(addr);
    clientSocket = 0;
}

ModbusTCPListener::~ModbusTCPListener() 
{
}

bool ModbusTCPListener::initSocket()
{
    int addrlen = sizeof(addr);
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == 0) {
        perror("Error creting socket");
        return false;
    }
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(port);
    if (bind(sock, (struct sockaddr*)&addr, (socklen_t)addrLenght) < 0) {
        perror("Error binding socket") ;
        return false;
    }
    listen(sock, 1);
    return true;
}

void ModbusTCPListener::acceptConnection()
{
    time_t time;
    
    if (clientSocket > 0)
        close(clientSocket);
    clientSocket = accept(sock, (struct sockaddr*)&addr, (socklen_t*)&addrLenght);
    time = std::time(0);
    struct tm* now = std::localtime(&time);
    DEBUG(std::cout << 
            (now->tm_hour) << ":" <<
            (now->tm_min) << ":" <<
            (now->tm_sec) << ". " <<
            "Accepted conenction: " << inet_ntoa(addr.sin_addr) << std::endl
            );
}

ssize_t ModbusTCPListener::recieveData(u_char *buf, ssize_t length)
{   
    return recv(clientSocket, buf, length, 0);
}

ssize_t ModbusTCPListener::sendData(u_char *buf, ssize_t length)
{   
    return send(clientSocket, buf, length, 0);
}

