/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Helper.cpp
 * Author: m
 * 
 * Created on 17 июня 2020 г., 11:57
 */

#include <iostream>

#include "Helper.h"

bool Helper::isDebug = true;

Helper::Helper() {
   
}

Helper::~Helper() {
}

void Helper::printHexString(u_char* buf, u_int16_t len, const std::string preText)
{
    std::cout << preText;
    for (int i=0; i<len; i++) {
        printf("%02X ", buf[i]);
    }
    printf("\n");
}
