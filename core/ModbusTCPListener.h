/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ModbusTCPListener.h
 * Author: m
 *
 * Created on 15 июня 2020 г., 17:20
 */

#ifndef MODBUSTCPLISTENER_H
#define MODBUSTCPLISTENER_H

#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "SerialPort.h"

#define DEBUG(a) if (Helper::isDebug) a;

class ModbusTCPListener 
{
    public:
        ModbusTCPListener(int port);
        virtual ~ModbusTCPListener();
        bool initSocket();
        void run();
        void acceptConnection();
        ssize_t recieveData(u_char *buf, ssize_t length);
        ssize_t sendData(u_char *buf, ssize_t length);
        
    private:    
        std::string ipAddr;
        int port;
        int sock;
        int addrLenght;
        struct sockaddr_in addr;
        int clientSocket;
};

#endif /* MODBUSTCPLISTENER_H */

