/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SerialPort.h
 * Author: m
 *
 * Created on 16 июня 2020 г., 14:31
 */

#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <stdio.h>
#include <fcntl.h>
#include <termios.h>
#include <iostream>
#include <unistd.h>

class SerialPort 
{
    public:
        SerialPort(bool debug = false): isDebug(debug) {};
        virtual ~SerialPort();
        bool openPort(const char tty[], int speed, int bits, char parity, int stopBits);
        void closePort();
        size_t readData(u_char* buf, size_t size);
        size_t writeData(u_char* buf, size_t size);
        
    private:
        int fileDescriptor;
        bool isDebug;
        

};

#endif /* SERIALPORT_H */

