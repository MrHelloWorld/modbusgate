/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ModbusPacketConverter.h
 * Author: m
 *
 * Created on 17 июня 2020 г., 11:50
 */

#ifndef MODBUSPACKETCONVERTER_H
#define MODBUSPACKETCONVERTER_H

#include <stdlib.h>
#include "Helper.h"

class ModbusPacketConverter 
{
    public:
        ModbusPacketConverter(bool debug = false): isDebug(debug) {};
        virtual ~ModbusPacketConverter();
        bool ConvertFromRtuToTcp(u_char srcPacket[], u_int16_t srcSize, u_char *dstPacket, u_int16_t& dstSize);
        bool ConvertFromTcpToRtu(u_char srcPacket[], u_int16_t srcSize, u_char *dstPacket, u_int16_t& dstSize);
        
    private:
        u_char* CalcCrc(u_char data[], u_int16_t dataSize);
        int currentTransactionID;
        bool isDebug;
};

#endif /* MODBUSPACKETCONVERTER_H */

