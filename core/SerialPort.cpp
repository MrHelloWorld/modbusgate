/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   SerialPort.cpp
 * Author: m
 *
 * Created on 16 июня 2020 г., 14:31
 */

#include <cstdint>
#include <linux/serial.h>
#include <sys/ioctl.h>

#include "SerialPort.h"

SerialPort::~SerialPort() {
}

bool SerialPort::openPort(const char tty[], int speed, int bits, char parity, int stopBits)
{
    struct termios options;
    struct serial_struct serial;
    speed_t sp;
    int ret;

    fileDescriptor = open(tty, O_RDWR | O_NOCTTY);
    if (fileDescriptor  == -1) {
        perror("Eror opening tty");
        return false;
    }
    tcgetattr(fileDescriptor, &options);
    switch (speed) {
        case 9600:
            sp = B9600;  break;
        case 19200:
            sp = B19200; break;
        case 38400:
            sp = B38400; break;
        case 57600:
            sp = B57600; break;
        case 115200:
            sp = B115200; break;
        default:
            std::cout << "Incorrect speed to set.";
            return false;
    };
    cfsetispeed(&options, sp);
    cfsetospeed(&options, sp);    
    
    options.c_cflag |= (CLOCAL | CREAD | FNDELAY );
    options.c_cflag &= ~CSIZE;
    switch (bits) {
        case 8:
            sp = CS8; break;
        case 7:
            sp = CS7; break;
        case 6:
            sp = CS6; break;
        case 5:
            sp = CS5; break;
        default:
            std::cout << "Incorrect number of mean bits to set.";
            return false;
    };
    options.c_cflag |= sp;
    switch (parity) {
        case 'n':
        case 'N':
            options.c_cflag &= ~PARENB; break;
        case 'o':
        case 'O':
            options.c_cflag |= PARENB;
            options.c_cflag |= PARODD;
            break;
        case 'e':
        case 'E':
            options.c_cflag |= PARENB; break;
        default:
            std::cout << "Incorrect number of mean bits to set.";
            return false;
    };
    if (stopBits ==1) {
        options.c_cflag &= ~CSTOPB;
    } else if (stopBits == 2) {
        options.c_cflag |= CSTOPB;
    } else {
        std::cout << "Incorrect number of mean bits to set.";
        return false;
    }
    /* setup for non-canonical mode */
    options.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    options.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    options.c_oflag &= ~OPOST;
    options.c_cc[VMIN] = 0;
    options.c_cc[VTIME] = 50;
    
    if (tcsetattr(fileDescriptor, TCSANOW, &options) != 0) {
        std::cout << "Error setting seral parameters.";
        return false;
    }
    ioctl(fileDescriptor, TIOCGSERIAL, &serial);
    serial.flags |= ASYNC_LOW_LATENCY;
    ret = ioctl(fileDescriptor, TIOCSSERIAL, serial);
    if (ret != 0) {
        std::cout << "Error setting port to low latency";        
    }
    return true;
}

size_t SerialPort::readData(u_char* buf, size_t size)
{
    size_t readLen;
    
    readLen = read(fileDescriptor, buf, size);
    if (readLen <= 0) {
        std::cout << "Error reading seral port\n";
    }
    return readLen;
}

size_t SerialPort::writeData(u_char* buf, size_t size)
{
    size_t writeLen;
    
    writeLen = write(fileDescriptor, buf, size);
    if (writeLen <= 0) {
        std::cout << "Error writing to seral port\n";
    }
    return writeLen;
}

void SerialPort::closePort()
{
    close(fileDescriptor);
}