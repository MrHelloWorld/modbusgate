/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Helper.h
 * Author: m
 *
 * Created on 17 июня 2020 г., 11:57
 */

#ifndef HELPER_H
#define HELPER_H

#include <stdlib.h>
#include <stdio.h>
#include <string>

class Helper 
{
    public:
        static void printHexString(u_char* buf, u_int16_t len, const std::string preText = "");
        static bool isDebug;
        Helper()  ;
        virtual ~Helper();
    private:

};

#endif /* HELPER_H */

