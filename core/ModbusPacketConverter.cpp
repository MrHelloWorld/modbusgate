/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   ModbusPacketConverter.cpp
 * Author: m
 *
 * Created on 17 июня 2020 г., 11:50
 */

#include <iostream>
#include <string.h>

#include "ModbusPacketConverter.h"

ModbusPacketConverter::~ModbusPacketConverter() {
}

bool ModbusPacketConverter::ConvertFromRtuToTcp(u_char srcPacket[], u_int16_t srcSize, u_char* dstPacket, u_int16_t& dstSize)
{
    dstPacket[0] = (currentTransactionID >> 8) & 0xFF;
    dstPacket[1] = currentTransactionID & 0xFF;
    dstPacket[2] = 0;
    dstPacket[3] = 0;
    dstPacket[4] = ((srcSize - 2) >> 8) & 0xFF;
    dstPacket[5] = (srcSize - 2) & 0xFF;
    memcpy(dstPacket + 6, srcPacket, srcSize - 2);
    dstSize = srcSize + 4;
}

bool ModbusPacketConverter::ConvertFromTcpToRtu(u_char srcPacket[], u_int16_t srcSize, u_char* dstPacket, u_int16_t& dstSize)
{
    int protocolId = srcPacket[2] << 8 + srcPacket[3];
    u_char *crc;

    if (protocolId != 0) {
        std::cout << "Wrong TCP packet recieved\n";
        return false;
    }
    currentTransactionID = (int)(srcPacket[0] << 8) + (int)srcPacket[1];
    dstSize = (u_int16_t)(srcPacket[4] << 8) + (u_int16_t)srcPacket[5];
    memcpy(dstPacket, srcPacket+6, (size_t)dstSize);
    crc = CalcCrc(dstPacket, dstSize);
    dstPacket[dstSize + 0] = crc[0];
    dstPacket[dstSize + 1] = crc[1];
    dstSize += 2;
}

u_char* ModbusPacketConverter::CalcCrc(u_char data[], u_int16_t dataSize)
{
    static u_char retCrc[2] = {255,255};
    u_int16_t crc = 0xFFFF;
    
    for (int i=0; i<dataSize; i++) {
        crc ^= data[i];
        for (int k=0; k<8; k++) {
            if (crc & 0x0001) {
                crc = crc >> 1;
                crc ^= 0xA001;
            }
            else
                crc = crc >> 1;
        }
    }
    retCrc[0] = crc & 0xFF;
    retCrc[1] = (crc>>8) & 0xFF;
    return retCrc;
}