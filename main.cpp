/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   main.cpp
 * Author: m
 *
 * Created on 15 июня 2020 г., 11:35
 */

#include <cstdlib>
#include <stdio.h>
#include <sys/socket.h>
#include <iostream>
#include <bits/getopt_core.h>
#include "core/ModbusTCPListener.h"
#include "core/Helper.h"
#include "core/ModbusPacketConverter.h"
#include <ctime>

#define DEBUG(a) if (Helper::isDebug) a;

struct GlobalArgs {
    int tcpPort;
    char* tty;
    int speed;
    u_char bits;
    char parity;
    u_char stopBits;
    int delay;
    bool debug;
};

static GlobalArgs globalArgs = {
    10001,
    NULL,
    115200,
    8,
    'N',
    1,
    100,
    false
};

void printUsage()
{
    std::cout << "modbusGate - gate between ModbusTCP and ModbusRTU devices\n";
    std::cout << "Usage options:\n";
    std::cout << "      -P: TCP port for incoming connections, eg. 10001\n";
    std::cout << "      -T: ModbusRTU interface, eg. /dev/ttyUSB0\n";
    std::cout << "      -S: ModbusRTU interface speed, eg. 9600, 19200 etc.\n";
    std::cout << "      -b: ModbusRTU interface num bits, eg. 7, 8 etc\n";
    std::cout << "      -p: ModbusRTU interface parity, eg. n(N) for None parity, e(E) for Even, o(O) for Odd\n";
    std::cout << "      -s: ModbusRTU interface num stop bits, 1 or 2\n";
    std::cout << "      -d: delay between writing and reading from tty port, ms\n";
    std::cout << "      -v: write all data to screen\n";
}

void proceedParams(const int argc, char** argv)
{
    int rez = 0;
    
    if (argc < 2) {
        printUsage();
        exit(-1);
    }
    while ( (rez = getopt(argc, argv, "P:T:S:b:p:s:d:v")) != -1) {
        switch (rez) {
            case 'P': 
                globalArgs.tcpPort = atoi(optarg); break;
            case 'T': 
                globalArgs.tty = optarg; break;
            case 'S': 
                globalArgs.speed = atoi(optarg); break;
            case 'b': 
                globalArgs.stopBits = atoi(optarg); break;
            case 'p': 
                globalArgs.parity = optarg[0]; break;
            case 's': 
                globalArgs.stopBits = atoi(optarg); break;
            case 'd': 
                globalArgs.delay = atoi(optarg); break;
            case 'v': 
                globalArgs.debug = true; break;
            case '?': 
                printUsage();
                exit(-1);
        }
    }
}

int main(int argc, char** argv)
{   
    SerialPort serial = SerialPort();
    u_char rtuPacket[256];
    u_int16_t rtuPacketSize;
    u_char tcpPacket[256];
    u_int16_t tcpPacketSize;
    time_t time;
    ModbusPacketConverter converter = ModbusPacketConverter();

    proceedParams(argc, argv);
    ModbusTCPListener listener = ModbusTCPListener(globalArgs.tcpPort);
    
    Helper::isDebug = globalArgs.debug;
    if (!serial.openPort(globalArgs.tty, globalArgs.speed, globalArgs.bits, globalArgs.parity, globalArgs.stopBits)) {
        std::cout << "Can't open serial port" << std::endl;
        exit(-1);
    }

    if (!listener.initSocket()) {
        std::cout << "Can't init sockets" << std::endl;
        exit(-1);
    }
    while (true) {
        listener.acceptConnection();
        while (true) {
            tcpPacketSize = listener.recieveData(tcpPacket, 512);
            if (tcpPacketSize <= 0) {
                time = std::time(0);
                struct tm* now = std::localtime(&time);
                
                DEBUG(std::cout << 
                    (now->tm_hour) << ":" <<
                    (now->tm_min) << ":" <<
                    (now->tm_sec) << ". " <<
                    "Connection lost. Awaiting new connection...\n"
                );
                break;
            }
            converter.ConvertFromTcpToRtu(tcpPacket, tcpPacketSize, rtuPacket, rtuPacketSize);
            serial.writeData(rtuPacket, rtuPacketSize);
            DEBUG(Helper::printHexString(tcpPacket, tcpPacketSize, "Recived TCP Packet:"));
            //DEBUG(Helper::printHexString(rtuPacket, rtuPacketSize, "Send RTU Packet   :"));
            usleep(1000*globalArgs.delay);
            rtuPacketSize = serial.readData(rtuPacket, 512);
            if (rtuPacketSize <= 2)  {
                DEBUG("Nothing was recieved\n");
                continue;
            }
            DEBUG(printf("Recieved %u bytes\n", rtuPacketSize));
            converter.ConvertFromRtuToTcp(rtuPacket, rtuPacketSize, tcpPacket, tcpPacketSize);
            //DEBUG(Helper::printHexString(rtuPacket, rtuPacketSize, "Recived RTU Packet:"));
            DEBUG(Helper::printHexString(tcpPacket, tcpPacketSize, "Send TCP Packet   :"));
            listener.sendData(tcpPacket, tcpPacketSize);
            DEBUG(std::cout << "-------------------------------\n");
        }
    }
    serial.closePort();
    return 0;
}

